package com.mmlab.mac;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends Activity
        implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    // LogCat tag
    private static final String TAG = MainActivity.class.getSimpleName();
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private Location mLastLocation;

    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;

    // boolean flag to toggle periodic location updates
    private boolean mRequestingLocationUpdates = false;

    private LocationRequest mLocationRequest;

    // Location updates intervals in sec
    private static int UPDATE_INTERVAL = 10000; // 10 sec
    private static int FATEST_INTERVAL = 5000; // 5 sec
    private static int DISPLACEMENT = 10; // 10 meters


    ProgressDialog progress;

    public static ArrayList<Store> storeArrayList;
    public static ArrayList<Food> foodArrayList;


    GridView gv;
    CustomStoreAdapter customStoreAdapter = null;
    SearchView edSearchStoreByName;
    Context context;

    String json_string = "";
    JSONObject jsonObject;
    JSONArray jsonArray;
    int postionStore = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        if (IsGPSEnabled(MainActivity.this) == false
                && IsInternetConnected(MainActivity.this) == false) {
            CheckInternetAndGPS();
            return;
        }

        //  CheckInternetAndGPS();
        progress = new ProgressDialog(MainActivity.this);

        edSearchStoreByName = (SearchView) findViewById(R.id.editSearchStoreByName);
        edSearchStoreByName.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                new BackgroundSearchStoreByName().execute(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        storeArrayList = new ArrayList<Store>();
        Log.d("jceation", "im here");
        if (checkPlayServices()) {
            buildGoogleApiClient();
        }
        Log.d("jceation", "im here-2");

        displayLocation();
        new GetMyLocation().execute();


        storeArrayList = new ArrayList<Store>();
        BackgroundTask backgroundTask = new BackgroundTask();
        backgroundTask.execute();
        /*
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        */
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Method to display the location on UI
     * */
    private boolean displayLocation() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    public void requestPermissions(@NonNull String[] permissions, int requestCode)
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
                Log.d("jceation","What the bug !!!");
                return false;
            }
        }
        mLastLocation = LocationServices.FusedLocationApi
                .getLastLocation(mGoogleApiClient);

        if (mLastLocation != null) {
            double latitude = mLastLocation.getLatitude();
            double longitude = mLastLocation.getLongitude();
            Log.v("ID","APP RESULT: Detect location succces");
            Log.v("ID","APP RESULT Lat:  "+mLastLocation.getLatitude());
            Log.v("ID","APP RESULT Long:  "+mLastLocation.getLongitude());
            return  true;

        } else {
            Log.v("ID", "APP RESULT: Detect location failed");
            return false;
        }
    }

    /**
     * Creating google api client object
     * */
    protected synchronized void buildGoogleApiClient() {
        Log.d("jceation","buildGoogleApiClient() here");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    /**
     * Method to verify google play services on the device
     * */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    public void onConnected(Bundle bundle) {
        // Once connected with google api, get the location
        displayLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = "
                + connectionResult.getErrorCode());
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        //CheckInternetAndGPS();
        checkPlayServices();

    }

    /*
     * Method to get current user's location,after that send that location to server
     * and get json contains the information of store near user.
     */
    public class GetMyLocation extends AsyncTask<Void,Void,Void>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            progress.setMessage("Đang lấy vị trí hiện tại...");
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            while (displayLocation()==false)
            {
                SystemClock.sleep(100);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            storeArrayList = new ArrayList<Store>();
            BackgroundTask backgroundTask = new BackgroundTask();
            backgroundTask.execute();
        }
    }


    public class BackgroundTask extends AsyncTask<Void, Void, String> {

        String json_url = "http://133.130.79.182/v1/api/getstoresnearby";
        String JSON_STRING;

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            //displayLocation();
            progress.setMessage("Đang lấy thông tin các quán gần bạn ...");
            progress.show();
            //Log.v("ID", "APP RESULT Lat:  " + mLastLocation.getLatitude());
            //Log.v("ID", "APP RESULT Long:  " + mLastLocation.getLongitude());
        }

        @Override
        protected String doInBackground(Void... params) {

            URL url= null;
            try {
                url = new URL(json_url);
                HttpURLConnection httpURLConnection=(HttpURLConnection)url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoInput(true);
                httpURLConnection.setDoOutput(true);

                OutputStream outputStream=httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter=new BufferedWriter(new OutputStreamWriter((outputStream)));
                String data= URLEncoder.encode("latitude","UTF-8")+"="+URLEncoder.encode(String.valueOf(mLastLocation.getLatitude()),"UTF-8")+"&"+
                        URLEncoder.encode("longitude","UTF-8")+"="+URLEncoder.encode(String.valueOf(mLastLocation.getLongitude()),"UTF-8");
                bufferedWriter.write(data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();


                InputStream inputStream=httpURLConnection.getInputStream();
                BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder stringBuilder=new StringBuilder();
                while ((JSON_STRING=bufferedReader.readLine())!=null)
                {
                    stringBuilder.append(JSON_STRING+"\n");
                }
                bufferedReader.close();;
                inputStream.close();;
                httpURLConnection.disconnect();
                return  stringBuilder.toString();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e)
            {

            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            json_string=aVoid;
            Toast.makeText(MainActivity.this,"Result: "+json_string,Toast.LENGTH_SHORT).show();;
            new GetStoreFromJson().execute();
        }
    }
    public class BackgroundSearchStoreByName extends AsyncTask<String, Void, String> {

        String json_url = "http://133.130.79.182/v1/api/getstoresbyname";
        String JSON_STRING;

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            //displayLocation();
            Toast.makeText(MainActivity.this,"APP RESULT: Search store by name",Toast.LENGTH_SHORT).show();
        }

        @Override
        protected String doInBackground(String... params) {

            URL url= null;
            try {
                url = new URL(json_url);
                HttpURLConnection httpURLConnection=(HttpURLConnection)url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoInput(true);
                httpURLConnection.setDoOutput(true);

                OutputStream outputStream=httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter=new BufferedWriter(new OutputStreamWriter((outputStream)));


                String data= URLEncoder.encode("store_name","UTF-8")+"="+URLEncoder.encode(params[0],"UTF-8");
                bufferedWriter.write(data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();


                InputStream inputStream=httpURLConnection.getInputStream();
                BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder stringBuilder=new StringBuilder();
                while ((JSON_STRING=bufferedReader.readLine())!=null)
                {
                    stringBuilder.append(JSON_STRING+"\n");
                }
                bufferedReader.close();;
                inputStream.close();;
                httpURLConnection.disconnect();
                return  stringBuilder.toString();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e)
            {

            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            json_string=aVoid;
           // Toast.makeText(MainActivity.this,"Result: "+json_string,Toast.LENGTH_SHORT).show();
            new GetStoreFromJson().execute();
        }
    }
    public class BackgroundGetMenuFromStoreID extends AsyncTask<String, Void, String> {

        String json_url = "http://133.130.79.182/v1/api/getmenubystoreid";
        String JSON_STRING;

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            //displayLocation();

        }

        @Override
        protected String doInBackground(String... params) {

            URL url= null;
            try {
                url = new URL(json_url);
                HttpURLConnection httpURLConnection=(HttpURLConnection)url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoInput(true);
                httpURLConnection.setDoOutput(true);

                OutputStream outputStream=httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter=new BufferedWriter(new OutputStreamWriter((outputStream)));


                String data= URLEncoder.encode("store_id","UTF-8")+"="+URLEncoder.encode(params[0],"UTF-8");
                bufferedWriter.write(data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();


                InputStream inputStream=httpURLConnection.getInputStream();
                BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder stringBuilder=new StringBuilder();
                while ((JSON_STRING=bufferedReader.readLine())!=null)
                {
                    stringBuilder.append(JSON_STRING+"\n");
                }
                bufferedReader.close();;
                inputStream.close();;
                httpURLConnection.disconnect();
                return  stringBuilder.toString();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e)
            {

            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            json_string=aVoid;
            new GetMenuFromJson().execute();
        }
    }




    public class GetStoreFromJson extends  AsyncTask<Void, Void, Void>
    {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            storeArrayList=new ArrayList<Store>();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                jsonObject=new JSONObject(json_string);
                jsonArray=jsonObject.getJSONArray("stores");
                int count=0;
                int id;
                int rating;
                String  name, location, description, hourStoreOpen;
                String open_time, close_time;
                Bitmap imageOfStore;
                Store store;
                while (count<jsonArray.length())
                {
                    JSONObject jsonObject =jsonArray.getJSONObject(count);
                    id=Integer.parseInt(jsonObject.getString("id"));
                    name=jsonObject.getString("name");
                    description=jsonObject.getString("description_vi");
                    location=jsonObject.getString("location");
                    open_time=jsonObject.getString("open_time");
                    close_time=jsonObject.getString("close_time");
                    hourStoreOpen= open_time+" - "+close_time;
                    imageOfStore=GetBitmapFromURL(jsonObject.getString("image").trim());
                    rating=jsonObject.getInt("rating");
                    store=new Store(id,name,description,location,hourStoreOpen,imageOfStore,rating);
                    storeArrayList.add(store);
                    Log.v("ID", "APP RESULT Store:  "+count );
                    count++;
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progress.dismiss();
            UpdateGridView();
            Geocoder gcd = new Geocoder(MainActivity.this, Locale.getDefault());
            List<Address> addresses = null;
            try {
                addresses = gcd.getFromLocation(mLastLocation.getLatitude(), mLastLocation.getLongitude(), 1);
                if (addresses.size() > 0)
                {
                    TextView textViewAddress=(TextView)findViewById(R.id.addressUser);
                    textViewAddress.setText(addresses.get(0).getAddressLine(1)+","+addresses.get(0).getAddressLine(2)+","+ addresses.get(0).getCountryName());
                }
                Log.v("ID","APP RESULT Name: "+addresses.get(0).getAddressLine(1)+" "+addresses.get(0).getAddressLine(2)+" "+addresses.get(0).getCountryName());
            } catch (IOException e) {
                e.printStackTrace();
                Log.v("ID", "APP RESULT - Không xác định đc địa điểm " );
            }

        }
    }


    public class GetMenuFromJson extends  AsyncTask<Void, Void, Void>
    {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            foodArrayList=new ArrayList<Food>();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                jsonObject=new JSONObject(json_string);
                jsonArray=jsonObject.getJSONArray("menu");
                int count=0;
                int id;
                int category;
                String name_vi;
                String name_en;
                String description_vi, description_en, unit,feature_comment,detail_comment;
                String created_time, updated_time;
                Bitmap image;
                int unit_price,rating,status;
                int del_flag;

                Bitmap imageOfStore;
                Food food;

                while (count<jsonArray.length())
                {

                    JSONObject jsonObject =jsonArray.getJSONObject(count);
                    id=jsonObject.getInt("id");
                    category=jsonObject.getInt("category_id");
                    name_vi=jsonObject.getString("name_vi");
                    name_en=jsonObject.getString("name_en");
                    description_vi=jsonObject.getString("description_vi");
                    description_en=jsonObject.getString("description_en");
                    image=GetBitmapFromURL(jsonObject.getString("image"));
                    unit=jsonObject.getString("unit");
                    unit_price=jsonObject.getInt("unit_price");
                    rating=jsonObject.getInt("rating");
                    feature_comment=jsonObject.getString("feature_comment");
                    detail_comment=jsonObject.getString("detail_comment");
                    status=jsonObject.getInt("status");
                    created_time=jsonObject.getString("created_time");
                    updated_time=jsonObject.getString("updated_time");
                    del_flag=jsonObject.getInt("del_flag");
                    food=new Food(image,name_vi,description_vi,unit_price,rating,unit);
                    foodArrayList.add(food);
                    Log.v("ID", "APP RESULT Menu:  "+count );
                    count++;
                }
                Log.v("ID","APP RESULT - Arraylist Menu Length: "+foodArrayList.size());
            }
            catch (JSONException e) {
                e.printStackTrace();

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progress.dismiss();
            //Log.v("ID","APP RESULT - MainActivity - Position 1.0");
            Intent intent=new Intent(MainActivity.this,FoodMenuDemoActivity.class);
            //Log.v("ID","APP RESULT - MainActivity - Position 1.1: "+foodArrayList.size());
            //intent.putParcelableArrayListExtra("DATA_MENU",foodArrayList);
            //Log.v("ID", "APP RESULT - MainActivity - Position 1.2");
            Log.v("ID","APP RESULT  - Position store: "+postionStore);
            intent.putExtra("POSITION_STORE",postionStore);
            startActivity(intent);
            //Log.v("ID", "APP RESULT - MainActivity - Position 1.3");
        }
    }

    public Bitmap GetBitmapFromURL(String src) {
        try {
            java.net.URL url = new java.net.URL(src);
            HttpURLConnection connection = (HttpURLConnection) url
                    .openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    public  boolean IsInternetConnected (Context ctx) {
        ConnectivityManager connectivityMgr = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifi = connectivityMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobile = connectivityMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        // Check if wifi or mobile network is available or not. If any of them is
        // available or connected then it will return true, otherwise false;
        if (wifi != null) {
            if (wifi.isConnected()) {
                return true;
            }
        }
        if (mobile != null) {
            if (mobile.isConnected()) {
                return true;
            }
        }
        return false;
    }
    public boolean IsGPSEnabled (Context mContext){
        LocationManager locationManager = (LocationManager)
                mContext.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public boolean CheckInternetAndGPS()
    {
        boolean IsInternetConnected=IsInternetConnected(MainActivity.this);
        boolean IsGPSEnabled=IsGPSEnabled(MainActivity.this);

        if(IsInternetConnected==false)
        {


            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("Lỗi")
                    .setMessage("Ứng dụng yêu cầu mạng để hoạt động. Bạn cần phải khởi động mạng trước khi sử dụng.")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent i = new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS);
                            startActivity(i);
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                           dialog.dismiss();

                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();

        }

        if(IsGPSEnabled==false)
        {


            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("Lỗi")
                    .setMessage("Ứng dụng yêu cầu dịch vụ GPS để hoạt động. Bạn cần phải khởi động dịch vụ trước khi sử dụng.")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }

        IsInternetConnected=IsInternetConnected(MainActivity.this);
        IsGPSEnabled=IsGPSEnabled(MainActivity.this);
        return IsGPSEnabled&& IsInternetConnected;
    }


    public void UpdateGridView()    {
        gv=(GridView) findViewById(R.id.girdviewStore);
        customStoreAdapter=new CustomStoreAdapter(MainActivity.this,R.layout.girdview_custom,storeArrayList);
        gv.setAdapter(customStoreAdapter);
        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(MainActivity.this,"Lấy về danh sách menu món ăn của quán có ID: "+storeArrayList.get(position).getId()+",Name: "+storeArrayList.get(position).getName(),Toast.LENGTH_SHORT).show();
                progress.setMessage("Đang lấy thông tin các món ăn của quán...");
                progress.show();
                postionStore=position;
                new BackgroundGetMenuFromStoreID().execute(String.valueOf(storeArrayList.get(position).getId()));
            }
        });
    }
}
