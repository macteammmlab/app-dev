package com.mmlab.mac;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pham Ngoc Phuoc on 4/12/2015.
 */
public class CustomStoreAdapter extends ArrayAdapter<Store> {

    Context context;
    int layoutResourceId;
    ArrayList<Store> storeArrayList;

    public CustomStoreAdapter(Context context, int resource, ArrayList<Store> object)
    {
        super(context, resource, object);

        this.context=context;
        this.layoutResourceId=resource;
        this.storeArrayList=object;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row=convertView;
        RecordHolder holder=null;

        if(row==null)
        {
            LayoutInflater inflater=((Activity)context).getLayoutInflater();
            row=inflater.inflate(layoutResourceId,parent,false);

            holder=new RecordHolder();
            holder.nameOfStore=(TextView)row.findViewById(R.id.nameOfStore);
            holder.openHourOfStore=(TextView)row.findViewById(R.id.hoursOpenOfStore);
            holder.imageOfStore=(ImageView)row.findViewById(R.id.imageStore);
            row.setTag(holder);
        }
        else
            holder=(RecordHolder)row.getTag();
        Store store=storeArrayList.get(position);
        holder.imageOfStore.setImageBitmap(store.getImage());
        holder.nameOfStore.setText(" "+store.getName());
        holder.openHourOfStore.setText(" "+store.getOpenHours());
        return row;

       // return super.getView(position, convertView, parent);
    }

    static  class RecordHolder{
        ImageView imageOfStore;
        TextView nameOfStore, openHourOfStore;
    }
}
