package com.mmlab.mac;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by Alienware M15x on 08/12/2015.
 */
public class FoodSumAdapter extends BaseAdapter {

    List<Food> foods;
    Context context;
    int layout;

    public FoodSumAdapter(Context context,int layout,List<Food> foods){

        this.context = context;
        this.layout = layout;
        this.foods = foods;
    }
    @Override
    public int getCount() {
       return foods.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;
        LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(layout,null);
        TextView tvFSName = (TextView) view.findViewById(R.id.tvFSName);
        TextView tvFSAmount= (TextView) view.findViewById(R.id.tvFSAmount);
        TextView tvFSPrice = (TextView) view.findViewById(R.id.tvFSPrice);

        Food temp = foods.get(position);
        tvFSName.setText(temp.getName_vi());
        tvFSAmount.setText("x " + temp.getQuatity());
        tvFSPrice.setText(temp.getUnit_price() + "");

        return view;
    }
}
