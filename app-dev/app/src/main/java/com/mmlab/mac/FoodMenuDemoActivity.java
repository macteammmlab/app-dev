package com.mmlab.mac;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.ArrayList;
import java.util.List;

import io.techery.properratingbar.ProperRatingBar;

/**
 * Created by Alienware M15x on 15/12/2015.
 */
public class FoodMenuDemoActivity extends Activity {

    ListView lvFoodMenu;
    ListView lvFoodSum;
    FoodMenuAdapter menuAdapter;
    FoodSumAdapter sumAdapter;
    String[] foodTestNames;
    List<Food> foodList;
    static List<Food> foodOrderList;
    SlidingUpPanelLayout suplFoodSum;
    TextView tvFoodTotal;

    TextView tvRestaurantName;
    ImageView ivRestaurantAvatar;
    ProperRatingBar prbRestaurantRating;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_menu);
        //get data from MainActivity
        initData();
        //test-value
        int positionOfStore=getIntent().getExtras().getInt("POSITION_STORE");
        Log.v("ID", "APP RESULT  - Position store: " + positionOfStore);
        //init GUI
        tvRestaurantName =(TextView)findViewById(R.id.tvRestaurantName);
        ivRestaurantAvatar=(ImageView)findViewById(R.id.ivRestaurantAvatar);
        prbRestaurantRating=(ProperRatingBar)findViewById(R.id.prbRestaurantRating);
        tvFoodTotal = (TextView) findViewById(R.id.tvFoodTotal);
        lvFoodSum = (ListView) findViewById(R.id.lvFoodSum);
        lvFoodMenu = (ListView) findViewById(R.id.lvFoodMenu);
        suplFoodSum = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);

        //restaurant infomation
        tvRestaurantName.setText(MainActivity.storeArrayList.get(positionOfStore).getName());
        ivRestaurantAvatar.setImageBitmap(MainActivity.storeArrayList.get(positionOfStore).getImage());
        prbRestaurantRating.setRating(MainActivity.storeArrayList.get(positionOfStore).getRating());

        //set up Sum lvFoodSum
        sumAdapter = new FoodSumAdapter(this,R.layout.listview_food_sum_item,foodOrderList);
        lvFoodSum.setAdapter(sumAdapter);

        //set up lvFoodMenu
        menuAdapter = new FoodMenuAdapter(getApplicationContext(),R.layout.listview_food_menu_item_2,foodList,foodOrderList,sumAdapter,tvFoodTotal);
        lvFoodMenu.setAdapter(menuAdapter);

        //set up suplFoodSum
        suplFoodSum.setPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                Log.d("jceation", "onPanelSlide-" + slideOffset);

            }

            @Override
            public void onPanelCollapsed(View panel) {
                Log.d("jceation", "onPanelCollapsed");
            }

            @Override
            public void onPanelExpanded(View panel) {
                Log.d("jceation", "onPanelExpanded");

            }

            @Override
            public void onPanelAnchored(View panel) {
                Log.d("jceation", "onPanelAnchored");
            }

            @Override
            public void onPanelHidden(View panel) {
                Log.d("jceation", "onPanelHidden");
            }
        });

    }

    private void initData() {
        foodList = new ArrayList<Food>();
        foodOrderList = new ArrayList<Food>();

        try{


            // Get ArrayList Bundle
            //foodList = getIntent().getParcelableArrayListExtra("DATA_MENU");
            //Log.v("ID","APP RESULT - FoodMenuDemoActivity - Receive intent success ! + Size:" +foodList.size());

            foodList=MainActivity.foodArrayList;

        } catch(Exception e){
            e.printStackTrace();
            Log.v("ID","APP RESULT - FoodMenuDemoActivity - Error when receive intent");
        }
        /*
        foodList.add(new Food(
                null,
                "Ga quay toa sang",
                "Thom qua troi qua dat",
                10000,
                4,
                "1 to"
        ));
        foodList.add(new Food(
                null,
                "Com chien duong chau",
                "Ba me don la ngon",
                12000,
                3,
                "1 to"
        ));
        foodList.add(new Food(
                null,
                "Bo bit tet",
                "Dang doi bung ...",
                10000,
                5,
                "1 to"
        ));
        foodList.add(new Food(
                null,
                "Vit quay Bac Kinh",
                "Tra tan bao tu bang mat",
                108000,
                2,
                "1 con"
        ));
        foodList.add(new Food(
                null,
                "Com chien trong tu lanh",
                "Lanh den tung hot com",
                25000,
                4,
                "1 to"
        ));
        foodList.add(new Food(
                null,
                "Pho kho Gia Lai",
                "Lai rai buoi chieu",
                32000,
                3,
                "1 to"
        ));
        foodList.add(new Food(
                null,
                "Trai cay dam",
                "An kem voi sua dac",
                25000,
                5,
                "1 to"
        ));
        foodList.add(new Food(
                null,
                "Kem tuyet Bingsu Han Quoc",
                "Khuyen mai cho FA",
                90000,
                5,
                "1 to"
        ));*/
    }
}
