package com.mmlab.mac;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public class Food implements Parcelable {
    private int id;
    private int category_id;
    private String name_vi;
    private String name_en;
    private String description_vi;
    private String description_en;
    private Bitmap image;
    private String unit;
    private int unit_price;
    private int rating;
    private String feature_comment;
    private String detail_comment;
    private int status;
    private String created_time;
    private String updated_time;
    int del_flag;

    //only for display
    private int quatity;
    public  Food(){
        image = null;
        description_vi = null;
        name_vi = null;
        unit_price = 0;
        rating = 0;
        quatity = 0;
    }
    public Food(Bitmap image,String name_vi,String description_vi,int unit_price,int rating,String unit)
    {
        this.image = image;
        this.name_vi = name_vi;
        this.description_vi = description_vi;
        this.unit_price = unit_price;
        this.rating = rating;
        this.quatity = 0;
        this.unit = unit;
    }

    public Food(int category_id, String created_time, int del_flag, String description_en, String description_vi, String detail_comment, String feature_comment, int id, Bitmap image, String name_en, String name_vi, int rating, int status, String unit, int unit_price, String updated_time) {
        this.id = id;
        this.image = image;
        this.name_en = name_en;
        this.name_vi = name_vi;
        this.rating = rating;
        this.status = status;
        this.unit = unit;
        this.unit_price = unit_price;
        this.updated_time = updated_time;
        this.category_id = category_id;
        this.created_time = created_time;
        this.del_flag = del_flag;
        this.description_en = description_en;
        this.description_vi = description_vi;
        this.detail_comment = detail_comment;
        this.feature_comment = feature_comment;
        //for display
        this.quatity = 0;
    }

    public Food(Parcel source)
    {
        Log.v("ID", "APP RESULT - Parcel constructor");
        this.id = source.readInt();
        Log.v("ID", "APP RESULT - Parcel constructor 1.1");

        this.image =source.readParcelable(null);
        Log.v("ID", "APP RESULT - Parcel constructor 1.2");
        this.name_en = source.readString();
        this.name_vi = source.readString();

        this.rating = source.readInt();
        this.status = source.readInt();
        this.unit = source.readString();

        this.unit_price = source.readInt();
        this.updated_time = source.readString();
        this.category_id = source.readInt();
        this.created_time = source.readString();
        Log.v("ID", "APP RESULT - Parcel constructor 1.3");
        this.del_flag = source.readInt();
        this.description_en = source.readString();
        this.description_vi = source.readString();
        this.detail_comment = source.readString();
        Log.v("ID", "APP RESULT - Parcel constructor 1.6");
        this.feature_comment = source.readString();
        this.quatity =source.readInt();


    }
    public int getQuatity(){

        return this.quatity;
    }
    public void setQuatity(int value){
        this.quatity = value;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public String getCreated_time() {
        return created_time;
    }

    public void setCreated_time(String created_time) {
        this.created_time = created_time;
    }

    public int getDel_flag() {
        return del_flag;
    }

    public void setDel_flag(int del_flag) {
        this.del_flag = del_flag;
    }

    public String getDescription_en() {
        return description_en;
    }

    public void setDescription_en(String description_en) {
        this.description_en = description_en;
    }

    public String getDescription_vi() {
        return description_vi;
    }

    public void setDescription_vi(String description_vi) {
        this.description_vi = description_vi;
    }

    public String getDetail_comment() {
        return detail_comment;
    }

    public void setDetail_comment(String detail_comment) {
        this.detail_comment = detail_comment;
    }

    public String getFeature_comment() {
        return feature_comment;
    }

    public void setFeature_comment(String feature_comment) {
        this.feature_comment = feature_comment;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public String getName_en() {
        return name_en;
    }

    public void setName_en(String name_en) {
        this.name_en = name_en;
    }

    public String getName_vi() {
        return name_vi;
    }

    public void setName_vi(String name_vi) {
        this.name_vi = name_vi;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public int getUnit_price() {
        return unit_price;
    }

    public void setUnit_price(int unit_price) {
        this.unit_price = unit_price;
    }

    public String getUpdated_time() {
        return updated_time;
    }

    public void setUpdated_time(String updated_time) {
        this.updated_time = updated_time;
    }

    public static  final Parcelable.Creator<Food> CREATOR = new Creator<Food>() {
        @Override
        public Food createFromParcel(Parcel source) {
            Food food=new Food(source);
            return  food;
        }

        @Override
        public Food[] newArray(int size) {
            return new Food[0];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.category_id);
        dest.writeString(name_en);
        dest.writeString(name_vi);
        dest.writeString(description_en);
        dest.writeString(description_vi);
        dest.writeParcelable(image,flags);
        dest.writeString(unit);
        dest.writeInt(unit_price);
        dest.writeInt(rating);
        dest.writeString(feature_comment);
        dest.writeString(detail_comment);
        dest.writeInt(status);
        dest.writeString(created_time);
        dest.writeString(updated_time);
        dest.writeInt(del_flag);
        dest.writeInt(quatity);
        Log.v("ID", "APP RESULT - Parcel writer");

    }
}
