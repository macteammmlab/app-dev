package com.mmlab.mac;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Alienware M15x on 04/12/2015.
 */
public class FoodMenuAdapter extends BaseAdapter {
    private Context context;
    private FoodSumAdapter sumAdapter;
    private TextView tvFoodTotal;

    int layout;
    List<Food> foods;
    static List<Food> orders;
    static List<Boolean> isQBoardOpenedList;
    static Animation animPressedBtn;
    public FoodMenuAdapter(Context context, int layout, List<Food> foods, List<Food> foodOrderList, FoodSumAdapter sumAdapter, TextView tvFoodTotal) {
        this.context = context;
        this.layout = layout;
        this.foods = foods;
        this.orders = foodOrderList;
        this.sumAdapter = sumAdapter;
        this.tvFoodTotal = tvFoodTotal;
        this.isQBoardOpenedList = new ArrayList<Boolean>();
        for(int i=0;i<foods.size();i++)
            isQBoardOpenedList.add(false); //init value
        animPressedBtn = AnimationUtils.loadAnimation(this.context,R.anim.anim_rotate_scale_pressed_btn);
    }

    @Override
    public int getCount() {
        return foods.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        //cach 1 - View Holder - still error

        ViewHolder viewHolder=null;
        if(convertView == null) {
            final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(layout, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);

        }
        else
            viewHolder= (ViewHolder) convertView.getTag();

        final Food food = foods.get(position);
        final ViewHolder finalViewHolder = viewHolder;
        viewHolder.llContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*int amount = Integer.parseInt(tvFoodAmount.getText().toString()) ;
                if(amount > 0 )
                    llContent.setBackgroundColor(ContextCompat.getColor(context,R.color.colorMudPrimmaryDark));
                else
                    llContent.setBackgroundColor(Color.WHITE);*/
                Boolean isOpened = isQBoardOpenedList.get(position);
                isQBoardOpenedList.set(position, isOpened ? false : true);

                notifyDataSetChanged();


            }
        });

        viewHolder.ivAmountUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(animPressedBtn);
                int amount = food.getQuatity();
                amount++;
                food.setQuatity(amount);
                finalViewHolder.tvFoodAmount.setText(amount + "");
                summary();
                if (!orders.contains(food))
                    orders.add(food);
                sumAdapter.notifyDataSetChanged();

            }
        });
        viewHolder.ivAmountDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(animPressedBtn);
                int amount = food.getQuatity();
                amount--;
                if (amount <= 0) {
                    amount = 0;
                    if (orders.contains(food))
                        orders.remove(food);
                }

                food.setQuatity(amount);
                summary();
                finalViewHolder.tvFoodAmount.setText(amount + "");
                sumAdapter.notifyDataSetChanged();
            }
        });



        viewHolder.tvName.setText(food.getName_vi());
        viewHolder.tvPrice.setText(food.getUnit_price() + "");
        viewHolder.tvDescription.setText(food.getDescription_vi());
        //tvCount.setText(temp.getAmount()+"");
        viewHolder.tvRatingNumber.setText(food.getRating() + "");
        viewHolder.tvUnit.setText(food.getUnit());

        if (isQBoardOpenedList.get(position)) {

            viewHolder.llAmountBoard.setVisibility(View.VISIBLE);
        }
        else {
            viewHolder.llAmountBoard.setVisibility(View.GONE);
        }

        if (food.getQuatity() > 0) {

            finalViewHolder.ivAvatar.setBackgroundColor(ContextCompat.getColor(context, R.color.colorGDGPrimmary));
            finalViewHolder.ivAvatar.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.tick_icon));
        } else {
            finalViewHolder.ivAvatar.setBackgroundColor(ContextCompat.getColor(context, R.color.colorIceBonus));
            if (food.getImage() != null)
                finalViewHolder.ivAvatar.setImageBitmap(food.getImage());
            else
                finalViewHolder.ivAvatar.setBackgroundColor(Color.argb(0, 0, 0, 0));
        }
        return convertView;

    }

    public void summary() {

        long sum = 0;
        for(Food f : foods)
        {
            sum += f.getQuatity() * f.getUnit_price();
        }
        tvFoodTotal.setText(context.getResources().getString(R.string.lbl_total)+"    "+sum);
    }
    private static class ViewHolder{
        ImageView ivAvatar ;
        TextView tvFoodAmount ;
        LinearLayout llAmountBoard ;
        TextView tvName ;
        TextView tvDescription;
        TextView tvPrice ;
        TextView tvRatingNumber;
        TextView tvUnit ;
        ImageView ivAmountUp ;
        ImageView ivAmountDown ;
        LinearLayout llFoodItem ;
        LinearLayout llContent;

        public ViewHolder(View view){

            tvName = (TextView) view.findViewById(R.id.tvFoodName);
            tvDescription = (TextView) view.findViewById(R.id.tvFoodDescription);
            tvPrice = (TextView) view.findViewById(R.id.tvFoodPrice);
            tvRatingNumber = (TextView) view.findViewById(R.id.tvRatingNumber);
            tvUnit = (TextView) view.findViewById(R.id.tvFoodUnit);
            ivAvatar = (ImageView) view.findViewById(R.id.ivPhoto);
            ivAmountUp = (ImageView) view.findViewById(R.id.ivAmountUp);
            ivAmountDown = (ImageView) view.findViewById(R.id.ivAmountDown);
            tvFoodAmount = (TextView) view.findViewById(R.id.tvFoodAmount);
            llAmountBoard = (LinearLayout) view.findViewById(R.id.llAmountBoard);
            llFoodItem = (LinearLayout) view.findViewById(R.id.llFoodItem);
            llContent = (LinearLayout) view.findViewById(R.id.llFoodContent);

        }

    }
}
