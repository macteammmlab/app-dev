package com.mmlab.mac;

import android.graphics.Bitmap;
import android.util.Log;

/**
 * Created by Pham Ngoc Phuoc on 1/12/2015.
 */
public class Store {
    int id;
    String name;
    String description;
    String location;
    String openHours;
    Bitmap image;
    int rating;

    public int getRating() {
        return rating;
    }

    public Store(int id, String name, String description,String location, String openHours, Bitmap bitmap,int rating)
    {
        this.setId(id);

        this.setName(name);
        this.setDescription(description);
        this.setLocation(location);
        this.setOpenHours(openHours);
        this.setImage(bitmap);
        this.rating=rating;
        Log.v("ID", "Position 1.1");
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    public void setRating(int rating) {
        this.rating = rating;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public String getOpenHours() {
        return openHours;
    }

    public void setOpenHours(String openHours) {
        this.openHours = openHours;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
