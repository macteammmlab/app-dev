package com.mmlab.mac;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by Alienware M15x on 25/12/2015.
 */
public class FoodMenuItemView extends LinearLayout{

    Animation animPressedBtn ;
    Context context;
    ImageView ivAvatar ;
    TextView tvFoodAmount ;
    LinearLayout llAmountBoard ;
    TextView tvName ;
    TextView tvDescription;
    TextView tvPrice ;
    TextView tvRatingNumber;
    TextView tvUnit ;
    ImageView ivAmountUp ;
    ImageView ivAmountDown ;
    LinearLayout llFoodItem ;
    LinearLayout llContent;
    Food food;

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        tvName = (TextView) findViewById(R.id.tvFoodName);
        tvDescription = (TextView) findViewById(R.id.tvFoodDescription);
        tvPrice = (TextView) findViewById(R.id.tvFoodPrice);
        tvRatingNumber = (TextView) findViewById(R.id.tvRatingNumber);
        tvUnit = (TextView) findViewById(R.id.tvFoodUnit);
        ivAvatar = (ImageView) findViewById(R.id.ivPhoto);
        ivAmountUp = (ImageView) findViewById(R.id.ivAmountUp);
        ivAmountDown = (ImageView) findViewById(R.id.ivAmountDown);
        tvFoodAmount = (TextView) findViewById(R.id.tvFoodAmount);
        llAmountBoard = (LinearLayout) findViewById(R.id.llAmountBoard);
        llFoodItem = (LinearLayout) findViewById(R.id.llFoodItem);
        llContent = (LinearLayout) findViewById(R.id.llFoodContent);

    }
    public void SetInfo(final Food food)
    {

        this.food = (food != null)?food:new Food();
        final Food finalFood = this.food;
        llContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (llAmountBoard.getVisibility() == View.GONE) {
                    llAmountBoard.setVisibility(View.VISIBLE);
                } else {
                    llAmountBoard.setVisibility(View.GONE);
                }
                if (finalFood.getQuatity() > 0) {

                    ivAvatar.setBackgroundColor(ContextCompat.getColor(context, R.color.colorGDGPrimmary));
                    ivAvatar.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.tick_icon));
                } else {
                    ivAvatar.setBackgroundColor(ContextCompat.getColor(context, R.color.colorIceBonus));
                    if (finalFood.getImage() != null)
                        ivAvatar.setImageBitmap(finalFood.getImage());
                    else
                        ivAvatar.setBackgroundColor(Color.argb(0, 0, 0, 0));
                }
            }
        });
        ivAmountUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(animPressedBtn);
                int amount = food.getQuatity();
                amount++;
                finalFood.setQuatity(amount);
                tvFoodAmount.setText(amount + "");
                /*summary();
                if (!orders.contains(finalFood))
                    orders.add(finalFood);
                sumAdapter.notifyDataSetChanged();*/

            }
        });
        ivAmountDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(animPressedBtn);
                int amount = finalFood.getQuatity();
                amount--;
                if (amount <= 0) {
                    amount = 0;
                   // if (orders.contains(finalFood))
                    //    orders.remove(finalFood);
                }

                finalFood.setQuatity(amount);
                //summary();
                tvFoodAmount.setText(amount + "");
                //sumAdapter.notifyDataSetChanged();
            }
        });

        tvName.setText(finalFood.getName_vi());
        tvPrice.setText(finalFood.getUnit_price() + "");
        tvDescription.setText(finalFood.getDescription_vi());
        //tvCount.setText(temp.getAmount()+"");
        tvRatingNumber.setText(finalFood.getRating() + "");
        tvUnit.setText(food.getUnit());

        if (finalFood.getImage() != null)
            ivAvatar.setImageBitmap(finalFood.getImage());

    }

    public FoodMenuItemView(Context context) {
        super(context);
        this.context = context;
        animPressedBtn = AnimationUtils.loadAnimation(this.context, R.anim.anim_rotate_scale_pressed_btn);
    }
    public FoodMenuItemView(Context context, AttributeSet attrs)
    {
        super(context,attrs);
        this.context = context;
        animPressedBtn = AnimationUtils.loadAnimation(this.context, R.anim.anim_rotate_scale_pressed_btn);
    }
    public FoodMenuItemView(Context context,AttributeSet attrs,int defStyle)
    {
        super(context,attrs,defStyle);
        this.context = context;
        animPressedBtn = AnimationUtils.loadAnimation(this.context, R.anim.anim_rotate_scale_pressed_btn);
    }
}
